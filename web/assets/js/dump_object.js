var map = null,
    placemark = null,
    hint_content = null,
    baloon_content = null,
    latitude = null,
    longitude = null,
    form = null,
    placemark_style = null;

ymaps.ready(function() {
  if ($('#map').length != 0) {
    form = $('#map_object_form');
    init_map();
    get_data_from_form();
    init_placemark_style();
    init_placemark();

  } else if ($('#static_map').length != 0) {
    init_placemark_style();
    init_static_map();    
  }
});

function init_map() {
  map = new ymaps.Map("map", {
              center: [56.859948,53.229047], 
              zoom: 12
            });
  map.events.add('click', function (e) {
    coords = e.get('coords');
    longitude = coords[0];
    latitude  = coords[1]
    set_data_from_map();
    init_placemark();
  });
}

function init_static_map() {
  map = new ymaps.Map("static_map", {
              center: [56.859948,53.229047], 
              zoom: 12
            });
  all_dump_objects.forEach(function(dump_object, index){
    placemark = new ymaps.Placemark([dump_object.longitude,dump_object.latitude], { 
              hintContent: dump_object.name, 
              balloonContent: dump_object.description
          }, placemark_style);
    map.geoObjects.add(placemark);
  });
}

function init_placemark_style() {
  placemark_style = {};
  placemark_style.iconLayout = 'default#image';
  placemark_style.iconImageHref = "/assets/images/dump_objects/map_icon.png";
}

function init_placemark() {
  if (placemark != null) { map.geoObjects.remove(placemark) }

  placemark = new ymaps.Placemark([longitude,latitude], { 
            hintContent: hint_content, 
            balloonContent: baloon_content
            }, placemark_style);

  map.geoObjects.add(placemark);
}

function get_data_from_form() {
  hint_content = form.find('#form_name').val();
  baloon_content = form.find('#form_description').val();
  longitude = form.find('#form_longitude').val();
  latitude = form.find('#form_latitude').val();
  init_placemark(); 
}

function set_data_from_map() {
  form.find('#form_name').val(hint_content);
  form.find('#form_description').val(baloon_content);
  form.find('#form_longitude').val(longitude);
  form.find('#form_latitude').val(latitude);
}

$(document).on('change', '#map_object_form',function() {
  get_data_from_form();  
});
