<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\ControlMass;

class SuggestionSystemController extends Controller
{
  public function indexAction() {
    $reasoner = new ControlMass;
    $mass = mt_rand(200000, 2000000);
    $area_size = mt_rand(1, 20) / 10;
    $result = $reasoner->comparison($area_size, $mass);

    return $this->render('suggestion_system/index.html.twig', [
          "result" => $result,
          "area_size" => $area_size,
          "mass" => $mass]);   
  }
}
