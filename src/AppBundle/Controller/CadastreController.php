<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Cadastre;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Request;

class CadastreController extends Controller
{

    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Cadastre');
        $items = $repo->findAll();

        return $this->render('cadastre/index.html.twig', [
          "items" => $items,
          ]);
    }

    public function newAction(Request $request) {
      $cadastre = new Cadastre;

      $form = $this->createFormBuilder($cadastre)
        ->setAction($this->generateUrl('cadastre_new_path'))
        ->setMethod('POST')
        ->add('name', TextType::class, ['label' => 'Название объекта'])
        ->add('description', TextType::class, ['label' => 'Описание объекта'])
        ->add('longitude', NumberType::class, ['label' => 'Долгота'])
        ->add('latitude', NumberType::class, ['label' => 'Широта'])
        ->add('save', SubmitType::class, ["label" => "Создать объект на карте"])
        ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $cadastre = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $em->persist($cadastre);
        $em->flush();

        $this->addFlash("notice", "Объект успешно создан");
        return $this->redirectToRoute('cadastre_index_path');
      }

      return $this->render('cadastre/new.html.twig', [
        "form" => $form->createView()]);
    }

    public function deleteAction($cadastre_id = null) {
      $em = $this->getDoctrine()->getManager();
      $cadastre = $em->getRepository('AppBundle:Cadastre')->find($cadastre_id);

      $em->remove($cadastre);
      $em->flush();   

      $this->addFlash("notice", "Кадастр успешно удалён");
      return $this->redirectToRoute('cadastre_index_path');
    }

    public function editAction($cadastre_id, Request $request) {
      $em = $this->getDoctrine()->getEntityManager();
      $cadastre = $em->getRepository('AppBundle:DumpObject')->find($cadastre_id);
      
      $form = $this->createFormBuilder($cadastre)
        ->setAction($this->generateUrl('cadastre_edit_path'))
        ->setMethod('POST')
        ->add('name', TextType::class, ['label' => 'Название объекта'])
        ->add('description', TextType::class, ['label' => 'Описание объекта'])
        ->add('longitude', NumberType::class, ['label' => 'Долгота'])
        ->add('latitude', NumberType::class, ['label' => 'Широта'])
        ->add('save', SubmitType::class, ["label" => "Создать объект на карте"])
        ->getForm();           

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $em->flush();
        $this->addFlash("notice", "Кадастр успешно обновлён");
        return $this->redirectToRoute('cadastre_index_path'); 
      }

      return $this->render('cadastre/edit.html.twig', [
        "form" => $form->createView()]);
    }
    
    public function suggestionPanelAction($cadastre_id, Request $request) {
      $em = $this->getDoctrine()->getManager();
      $cadastre = $em->getRepository('AppBundle:Cadastre')->find($cadastre_id);

      return $this->render('cadastre/suggestion_panel.html.twig');
    }
}
