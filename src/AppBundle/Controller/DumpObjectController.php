<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\DumpObject;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Request;

class DumpObjectController extends Controller
{

    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:DumpObject');
        $items = $repo->findAll();

        return $this->render('dump_object/index.html.twig', [
          "items" => $items,
          ]);
    }

    public function newAction(Request $request) {
      $dump_object = new DumpObject;

      $form = $this->createFormBuilder($dump_object)
        ->setAction($this->generateUrl('dump_object_new'))
        ->setMethod('POST')
        ->add('name', TextType::class, ['label' => 'Название свалки'])
        ->add('description', TextType::class, ['label' => 'Описание свалки'])
        ->add('longitude', NumberType::class, ['label' => 'Долгота'])
        ->add('latitude', NumberType::class, ['label' => 'Широта'])
        ->add('save', SubmitType::class, ["label" => "Создать свалку на карте"])
        ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $dump_object = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $em->persist($dump_object);
        $em->flush();

        $this->addFlash("notice", "Свалка успешно создана");
        return $this->redirectToRoute('dump_object_index');
      }

      return $this->render('dump_object/new.html.twig', [
        "form" => $form->createView()]);
    }

    public function deleteAction($dump_object_id = null) {
      $em = $this->getDoctrine()->getEntityManager();
      $dump_object = $em->getRepository('AppBundle:DumpObject')->find($dump_object_id);

      $em->remove($dump_object);
      $em->flush();   

      $this->addFlash("notice", "Свалка успешно удалёна");
      return $this->redirectToRoute('dump_object_index'); 
    }

    public function editAction($dump_object_id, Request $request) {
      $em = $this->getDoctrine()->getEntityManager();
      $dump_object = $em->getRepository('AppBundle:DumpObject')->find($dump_object_id);
      
      $form = $this->createFormBuilder($dump_object)
        ->setMethod('POST')
        ->add('name', TextType::class, ['label' => 'Название свалки'])
        ->add('description', TextType::class, ['label' => 'свалки объекта'])
        ->add('longitude', NumberType::class, ['label' => 'Долгота'])
        ->add('latitude', NumberType::class, ['label' => 'Широта'])
        ->add('save', SubmitType::class, ["label" => "Обновить"])
        ->getForm();           

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $em->flush();
        $this->addFlash("notice", "Свалка успешно обновлёна");
        return $this->redirectToRoute('dump_object_index'); 
      }

      return $this->render('dump_object/edit.html.twig', [
        "form" => $form->createView()]);
    }
}
