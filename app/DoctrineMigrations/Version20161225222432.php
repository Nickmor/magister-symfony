<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161225222432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
      $table = $schema->createTable('map_object');
      $table->addColumn('id', 'integer');
      $table->addColumn('name', 'string');
      $table->addColumn('description', 'text');
      $table->addColumn('longitude', 'float');
      $table->addColumn('latitude', 'float');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
      $schema->dropTable('map_object');
    }
}
