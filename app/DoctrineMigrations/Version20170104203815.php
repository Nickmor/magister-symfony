<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170104203815 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('cadastre');
        $table->addColumn('id', 'integer');
        $table->addColumn('name', 'string');
        $table->addColumn('description', 'text');
        $table->addColumn('longitude', 'float');
        $table->addColumn('latitude', 'float');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
      $schema->dropTable('cadastre');
    }
}
