<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use AppBundle\Entity\DumpObject;
use AppBundle\Entity\Cadastre;


/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170104210356 extends AbstractMigration implements ContainerAwareInterface
{

    private $container;

    private $dump_objects;
    private $cadstres;


    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $dump_objects_repo = $em->getRepository('AppBundle:DumpObject');
        $this->dump_objects = $dump_objects_repo->findAll();

        foreach ($this->dump_objects as $key => $dump_object) {
            $em->remove($dump_object);
            $em->flush();
        }

        $cadastres_repo = $em->getRepository('AppBundle:Cadastre');
        $this->cadastres = $cadastres_repo->findAll();

        foreach ($this->cadastres as $key => $cadastre) {
            $em->remove($cadastre);
            $em->flush();
        }

        $this->addSql('ALTER TABLE cadastre ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE cadastre MODIFY COLUMN id INT auto_increment');
        $this->addSql('ALTER TABLE dump_object ADD PRIMARY KEY (id)');     
        $this->addSql('ALTER TABLE dump_object MODIFY COLUMN id INT auto_increment');     

    }

    public function postUp(Schema $schema) {
        $em = $this->container->get('doctrine.orm.entity_manager');

        foreach ($this->dump_objects as $key => $dump_object) {
            $new_dump_object = new DumpObject;
            $new_dump_object->setName($dump_object->getName());
            $new_dump_object->setDescription($dump_object->getDescription());
            $new_dump_object->setLongitude($dump_object->getLongitude());
            $new_dump_object->setLatitude($dump_object->getLatitude());

            $em->persist($new_dump_object);
            $em->flush();
        }
        foreach ($this->cadastres as $key => $cadastre) {
            $new_cadastre = new Cadastre;
            $new_cadastre->setName($cadastre->getName());
            $new_cadastre->setDescription($cadastre->getDescription());
            $new_cadastre->setLongitude($cadastre->getLongitude());
            $new_cadastre->setLatitude($cadastre->getLatitude());

            $em->persist($new_cadastre);
            $em->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $cadastre_table = $schema->getTable('cadastre');
        $cadastre_table->dropPrimaryKey( 'id' );
        $cadastre_table->addColumn('id', 'integer');

        $dump_object_table = $schema->getTable('dump_object');
        $dump_object_table->dropPrimaryKey('id');   
        $dump_object_table->addColumn('id', 'integer');
    }
}
