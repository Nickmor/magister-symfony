<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170103212004 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $schema->renameTable('map_object', 'dump_object');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->renameTable('dump_object', 'map_object');
    }
}
